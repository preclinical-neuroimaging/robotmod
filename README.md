Robotmod README guidance

Welcome to join us building semi-naturalistic environment for rats behavioral tests

Materials Preparation
```
Plexiglas (methyl methacrylate) plates
Polyacetal Straight gear & rack
NEMA17 step motor
Arduino Uno & Jumper cables & breadboard & pushbutton & interface L298N DC Motor Driver
Adjustable power supply
Raspberry Pi 3 Model B+ & Raspberry Pi Night Vision Camera
```
Cage setup
```
build the cage according to schematics:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/blob/main/assets/setup.png

connect the Arduino and Raspberry Pi according to schematics:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/blob/main/assets/electronic_connection.png

load Arduino code to Arduino Uno to control the movement of Robot:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/raw/main/codes/arduino_NEMA17_stepmotor.ino

load Python code to Raspberry Pi to control the video recording/rat detection:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/raw/main/codes/Python_code.py
```
experiment procedures
```
1. turn off the bright light, turn on the red light, put animal in nesting area.
2. Turn on RaspberryPi and Arduino.
3. Turn on adjustable power supply to power the motor. (Check the Voltage and current are         capable with motor and motor driver!)
4. 3 mins after putting the rat in nesting area, start recording.

-For habituation please check procedure 5;
-For baseline training please check procedure 6;
-For Robot testing please check procedure 7;

5. Put three food pellet (each 0.8-1.3 gram, grain based) in nesting area and leave rats in       nesting area for 30 mins.
6. Each rat has 3 trails, for each trail,  1 food pellet would be placed at 25.4cm, 50.8cm or     76.2cm away from nesting area in sequence. Open the gate and give rat 5 min to forage the      food. Once the rat successfully take the food back to nesting area, the gate should be         closed and trail end. If the rat failed, guide the rat back to nesting area and then close     the gate.
7. Each rat has 5 trails, for each trail, 1 food pellet would be placed at 25.4cm, 50.8cm,        76.2cm, 101.6cm or 127cm away from nesting area in sequence. Open the gate and give rat 3      min to forage the food. Every time the animals are about to get the food pellet (1-3cm away    from food pellet), the Robot will be triggered. Once animals successfully take the food back    nesting area, the gate will be closed and the trail end. If the rat failed, guide the rat      back to nesting area and then close the gate.
```
Statistics
```
Raw data could be collected by either manual recording during experimental phase, or throught recorded video after test.

Orgnize data the same format as:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/raw/main/robotmod_data.csv

install & load packages, and analyze data using R codes:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/raw/main/codes/stasitic.Rmd
```
Statistic explanation
```
The code could be downloaded:
https://gitlab.socsci.ru.nl/preclinical-neuroimaging/robotmod/-/raw/main/codes/stasitic.Rmd

1st, data will be converted into Rank - score value:
if failed ~ 7,
if sec >= 200 ~ 6,
if sec >= 100 ~ 5,
if sec >= 50 ~ 4,
if sec >= 25  ~ 3,
if sec >= 12.5 ~ 2,
if sec >= 6.25 ~ 1,
if sec <6.25 ~ 0.

2nd, the Model comparation by compare_performance(mod.sec, mod.score).
normality and heteroscedasticity are checked by check_normality() and check_heteroscedasticity(). Through this section we found model with scores seem to better fit into our liner model.

3rd, the Age effect is examed by using data from SD rats_foraging_under 90% food restriction_both sex.

4th, the Restriction effect is examed by using data from LE rats_foraging_under 45% and 60% food restriction_both sex.

5th, the Sex effect is examed by using data from different ages, food restrictions.

6th, the Sex effect is also examed by using data from Baseline day6 (the last day of Baseline training), to reveal the sex effect without threat.

7th, leaving and approaching behaviors are examed the same as procedure 4-6.
```