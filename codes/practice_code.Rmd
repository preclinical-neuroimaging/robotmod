## packages loading
```{r}
#General utility packages
library(tidyverse)
library(glue)
library(knitr)
library(data.table)
#Stats packages
library(lme4)
library(multcomp)
library(parameters)
library(effectsize)
library(performance)
library(emmeans)
#Plot packages
library(ggpubr)
```

```{r}
df <- read_csv('robotmod_data.csv') %>% 
          mutate(trials = as.factor(trials)) %>% 
          mutate(group = as.factor(group)) %>% 
          mutate(sex = as.factor(sex)) %>% 
          mutate(rat_ID = as.factor(rat_ID)) %>% 
          mutate(restriction = as.factor(restriction))%>%
          mutate(age = as.factor(age))%>%
          mutate(food.distance.cm = as.factor(food.distance.cm))%>%
         
        # Create score based on time. Following log attribution of score as a function of time. 
          mutate(leaving.score =  case_when(is.na(leaving.sec) ~ 7,
                                                  leaving.sec >= 200 ~ 6,
                                                  leaving.sec >= 100 ~ 5,
                                                  leaving.sec >= 50 ~ 4,
                                                  leaving.sec >= 25  ~ 3,
                                                  leaving.sec >= 12.5 ~ 2,
                                                  leaving.sec >= 6.25 ~ 1,
                                                  leaving.sec <6.25 ~ 0)) %>% 
            mutate(approaching.score =  case_when(is.na(approaching.sec) ~ 7,
                                                  approaching.sec >= 200 ~ 6,
                                                  approaching.sec >= 100 ~ 5,
                                                  approaching.sec >= 50 ~ 4,
                                                  approaching.sec >= 25  ~ 3,
                                                  approaching.sec >= 12.5 ~ 2,
                                                  approaching.sec >= 6.25 ~ 1,
                                                  approaching.sec <6.25 ~ 0)) %>% 
            mutate(backing.score =  case_when(is.na(backing.sec) ~ 7,
                                                  backing.sec >= 200 ~ 6,
                                                  backing.sec >= 100 ~ 5,
                                                  backing.sec >= 50 ~ 4,
                                                  backing.sec >= 25  ~ 3,
                                                  backing.sec >= 12.5 ~ 2,
                                                  backing.sec >= 6.25 ~ 1,
                                                  backing.sec <6.25 ~ 0))  %>%
            mutate(foraging.score =  case_when(is.na(foraging.sec) ~ 7,
                                                  foraging.sec >= 200 ~ 6,
                                                  foraging.sec >= 100 ~ 5,
                                                  foraging.sec >= 50 ~ 4,
                                                  foraging.sec >= 25  ~ 3,
                                                  foraging.sec >= 12.5 ~ 2,
                                                  foraging.sec >= 6.25 ~ 1,
                                                  foraging.sec <6.25 ~ 0))

#head(df) %>% kable("pipe")

```

```{r}
df.SD_Rday1 <- df %>% filter(df$robot.day == 1, df$group == "SD")

#summarize the percentage of foraging.score != 7 for each age in df.SD_Rday1
SD_Rday1_sum<-df.SD_Rday1 %>% group_by(trials, age) %>% summarize(percentage = sum(foraging.score != 7)/n())

fig1a <-ggbarplot(SD_Rday1_sum, x="trials", y="percentage",
  color = "age", palette = c("#7fc97f", "#fdc086"),
  size = 2, ylab = "foraging success [%]", 
  position = position_dodge(1))



fig1b<- ggplot(df.SD_Rday1, aes(x=trials, y=foraging.score, group=rat_ID)) + geom_line(color="gray", alpha=0.2)+
stat_summary(fun=mean, aes(group = age, color= age), geom="line", linewidth=2)+
stat_summary(fun.data = mean_se, aes(group=age, color = age), geom = "errorbar", alpha=1, width=0.05)+
scale_color_manual(values = c("#7fc97f", "#fdc086"))+
scale_y_continuous(breaks = seq(0, 7, by = 1))+
labs(x = "positions", y = "foraging score") + theme_classic() + facet_wrap(~age, ncol=2)

Fig1a_1b <- ggarrange(fig1a, fig1b, ncols=2,labels=c('A','B'), common.legend=TRUE,legend="right", widths=c(1,2))
Fig1a_1b
ggsave('C:/Users/xiamen/Desktop/modified robogator/result figs/Fig1a_1b.svg', plot = Fig1a_1b, device = 'svg',dpi = 300, width = 176, height = 140, units = "mm")

```


