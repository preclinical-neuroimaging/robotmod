from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QCheckBox
from PyQt5.Qt import QLineEdit
from PyQt5.QtCore import Qt
import sys
import cv2                                                                                       #Imports the OpenCv packages
import numpy as np                                                                               #Imports the Numpy packages and calls it np
import datetime
import serial

threshold = 250.00
#Define the threshold For the movement detection Higher number means more movement is needed
red = (0, 0, 255)                                                                                #Define the color Red
green = (0, 255, 0)
name = ""
RecordPath = '/home/pi/Desktop/savevideo/'            
videoname = RecordPath + name +'.avi'

arduino = serial.Serial('/dev/ttyACM0', 9600)

class Ui_MainWindow(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)  #

        self.timer_camera = QtCore.QTimer()  # set timer for Video frame rate
        self.cap = cv2.VideoCapture()  # video

        self.CAM_NUM = 0  # webcam

        self.left = 300
        self.top = 150
        self.width = 800
        self.height = 800

        self.X = False

        self.set_ui()  # initiate
        self.slot_init()  #
        self.ROIstate=0
    '''Program interface layout'''

    def set_ui(self):
        self.__layout_main = QtWidgets.QHBoxLayout()  # general layout
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.__layout_fun_button = QtWidgets.QVBoxLayout()  # buttons layout
        self.__layout_data_show = QtWidgets.QVBoxLayout()  # video layout
        self.button_entername = QtWidgets.QPushButton('enter name')  # enter name
        self.button_open_camera = QtWidgets.QPushButton('open camera')  # open video button
        self.button_save = QtWidgets.QPushButton('Record')
        self.button_close = QtWidgets.QPushButton('quit')  # quit button

        self.ROIAcheckbox = QCheckBox("ROIA", self)
        self.ROIBcheckbox = QCheckBox("ROIB", self)
        self.ROICcheckbox = QCheckBox("ROIC", self)
        self.ROIDcheckbox = QCheckBox("ROID", self)
        self.ROIEcheckbox = QCheckBox("ROIE", self)

        self.textbox = QLineEdit(self)

        self.textbox.resize(140, 40)

        self.button_open_camera.setMinimumHeight(50)  # size of button
        self.button_close.setMinimumHeight(50)
        self.button_entername.setMinimumHeight(50) # enter name
        self.button_save.setMinimumHeight(50)
        self.textbox.setMinimumHeight(50)

        self.textbox.move(10, 0)





        '''information display'''
        self.label_show_camera = QtWidgets.QLabel()  # define video Label
        self.label_show_camera.setFixedSize(640, 480)  # set video Label size640x480

        '''add button into layout'''
        self.__layout_fun_button.addWidget(self.button_entername)
        self.__layout_fun_button.addWidget(self.button_save)
        self.__layout_fun_button.addWidget(self.ROIAcheckbox)
        self.__layout_fun_button.addWidget(self.ROIBcheckbox)
        self.__layout_fun_button.addWidget(self.ROICcheckbox)
        self.__layout_fun_button.addWidget(self.ROIDcheckbox)
        self.__layout_fun_button.addWidget(self.ROIEcheckbox)
        self.__layout_fun_button.addWidget(self.button_open_camera)
        self.__layout_fun_button.addWidget(self.button_close)  #

        '''Add some controls to the overall layout'''
        self.__layout_main.addLayout(self.__layout_fun_button)  #
        self.__layout_main.addWidget(self.label_show_camera)  #

        '''After the general layout is arranged, you can pass the general layout as a parameter to the following function'''
        self.setLayout(self.__layout_main)  # display all the controls

    '''Initialize all slot functions'''

    def slot_init(self):
        self.button_open_camera.clicked.connect(self.button_open_camera_clicked)  #
        self.timer_camera.timeout.connect(self.show_camera)  #
        self.button_close.clicked.connect(self.button_close_clicked)  #
        self.ROIAcheckbox.stateChanged.connect(self.ROIAcheckbox_clicked)
        self.ROIBcheckbox.stateChanged.connect(self.ROIBcheckbox_clicked)
        self.ROICcheckbox.stateChanged.connect(self.ROICcheckbox_clicked)
        self.ROIDcheckbox.stateChanged.connect(self.ROIDcheckbox_clicked)
        self.ROIEcheckbox.stateChanged.connect(self.ROIEcheckbox_clicked)
        self.button_entername.clicked.connect(self.button_entername_clicked)
        self.button_save.clicked.connect(self.button_save_clicked)
    ''''''

    def button_open_camera_clicked(self):
        if self.timer_camera.isActive() == False:  #
            flag = self.cap.open(self.CAM_NUM)  # webcam
            if flag == False:  #
                QtWidgets.QMessageBox.warning(self, 'warning', "check the camera link with laptop", buttons=QtWidgets.QMessageBox.Ok)
            else:
                self.timer_camera.start(30)  # get frame every 30ms from camera
                self.button_open_camera.setText('close camera')
        else:
            self.timer_camera.stop()  #
            self.cap.release()  #
            self.label_show_camera.clear()  # clear video play area
            self.button_open_camera.setText('open camera')
            self.X = False
            self.button_save.setText('Record')

    def show_camera(self):
        flag, self.image = self.cap.read()  #
        show = cv2.resize(self.image, (640, 480))  # resize to 640x480
        _, previous = self.cap.read()  # Save a frame of the webcam
        _, current = self.cap.read()
        currenta = current[406:501,89:184]
        currentb = current[405:500,138:232]
        currentc = current[404:500,187:280]
        currentd = current[403:499,235:327]
        currente = current[406:502,327:414]
        previous_croppeda = previous[406:501, 89:184]
        previous_croppedb = previous[405:500,138:232]
        previous_croppedc = previous[404:500,187:280]
        previous_croppedd = previous[403:499,235:327]
        previous_croppede = previous[406:502,327:414]
        erra = np.sum((previous_croppeda.astype("float") - currenta.astype("float")) ** 2)  # Perfomes the MSE formula
        erra /= float(previous_croppeda.shape[0] * previous_croppeda.shape[1])
        errb = np.sum((previous_croppedb.astype("float") - currentb.astype("float")) ** 2)  # Perfomes the MSE formula
        errb /= float(previous_croppedb.shape[0] * previous_croppedb.shape[1])
        errc = np.sum((previous_croppedc.astype("float") - currentc.astype("float")) ** 2)  # Perfomes the MSE formula
        errc /= float(previous_croppedc.shape[0] * previous_croppedc.shape[1])
        errd = np.sum((previous_croppedd.astype("float") - currentd.astype("float")) ** 2)  # Perfomes the MSE formula
        errd /= float(previous_croppedd.shape[0] * previous_croppedd.shape[1])
        erre = np.sum((previous_croppede.astype("float") - currente.astype("float")) ** 2)  # Perfomes the MSE formula
        erre /= float(previous_croppede.shape[0] * previous_croppede.shape[1])
        if self.ROIstate==0:
                print('please select ROI')

        if self.ROIstate==1:
                if (erra >= threshold):  # Checks if the err is bigger or equal to the treshold
                        print(erra)  # Prints the Err
                        print("Area A Movement!")  # Prints that there is movement
                        points = np.array([[[406,89], [406, 184], [501, 184], [501, 89]]],
                              np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, red, thickness=2)
                        arduino.write(b'1')
                if (erra < threshold):  # Checks if the err is smaller then the treshold
                        points = np.array([[[406,89], [406, 184], [501, 184], [501, 89]]],
                              np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, green, thickness=2)

        if self.ROIstate==2:
                if (errb >= threshold):  # Checks if the err is bigger or equal to the treshold
                        print(erra)  # Prints the Err
                        print("Area B Movement!")  # Prints that there is movement
                        points = np.array([[[405, 138], [405, 232], [500, 232], [500, 138]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, red, thickness=2)
                        arduino.write(b'1')
                if (errb < threshold):  # Checks if the err is smaller then the treshold
                        points = np.array([[[405, 138], [405, 232], [500, 232], [500, 138]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, green, thickness=2)

        if self.ROIstate==3:
                if (errc >= threshold):  # Checks if the err is bigger or equal to the treshold
                        print(erra)  # Prints the Err
                        print("Area C Movement!")  # Prints that there is movement
                        points = np.array([[[404, 187], [404, 280], [500, 280], [500, 187]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, red, thickness=2)
                        arduino.write(b'1')
                if (errc < threshold):  # Checks if the err is smaller then the treshold
                        points = np.array([[[404, 187], [404, 280], [500, 280], [500, 187]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, green, thickness=2)

        if self.ROIstate==4:
                if (errd >= threshold):  # Checks if the err is bigger or equal to the treshold
                        print(erra)  # Prints the Err
                        print("Area D Movement!")  # Prints that there is movement
                        points = np.array([[[403, 235], [403, 327], [499, 327], [499, 235]]], np.int32)
                        cv2.polylines(previous, [points], True, red, thickness=2)
                        arduino.write(b'1')
                if (errd < threshold):  # Checks if the err is smaller then the treshold
                        points = np.array([[[403, 235], [403, 327], [499, 327], [499, 235]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, green, thickness=2)

        if self.ROIstate==5:
                if (erre >= threshold):  # Checks if the err is bigger or equal to the treshold
                        print(erra)  # Prints the Err
                        print("Area E Movement!")  # Prints that there is movement
                        points = np.array([[[406, 327], [406, 414], [502, 414], [502, 327]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, red, thickness=2)
                        arduino.write(b'1')
                if (erre < threshold):  # Checks if the err is smaller then the treshold
                        points = np.array([[[406, 327], [406, 414], [502, 414], [502, 327]]],
                                          np.int32)  # Defines the points for a square where the detection area should be on the full version of the frame
                        cv2.polylines(previous, [points], True, green, thickness=2)


        show = cv2.cvtColor(previous, cv2.COLOR_BGR2RGB)  # color back to RGB
        showImage = QtGui.QImage(show.data, show.shape[1], show.shape[0], QtGui.QImage.Format_RGB888)  #
        self.label_show_camera.setPixmap(QtGui.QPixmap.fromImage(showImage))  # display Qimage
        if self.X == True:
            out.write(self.image)


    def button_save_clicked(self):
        global out
        fourcc = cv2.VideoWriter_fourcc(*'XVID')
        out = cv2.VideoWriter(videoname, fourcc, 10.0, (640, 480), True)
        self.X = True
        self.button_save.setText('Recording...')



    def button_entername_clicked(self):
        global name
        global videoname
        name = (self.textbox.text())
        time = datetime.datetime.now().strftime("%Y%m%d_%H%M")
        videoname = RecordPath + time + name + '.avi'

    def button_close_clicked(self):
        self.close()

    def ROIAcheckbox_clicked(self,state):
        if state==Qt.Checked:
                self.ROIstate=1

    def ROIBcheckbox_clicked(self, state):
        if state == Qt.Checked:
                self.ROIstate=2

    def ROICcheckbox_clicked(self, state):
        if state == Qt.Checked:
                self.ROIstate = 3

    def ROIDcheckbox_clicked(self, state):
        if state == Qt.Checked:
                self.ROIstate = 4

    def ROIEcheckbox_clicked(self, state):
        if state == Qt.Checked:
                self.ROIstate = 5

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)  #
    ui = Ui_MainWindow()  #
    ui.show()  #
    sys.exit(app.exec_())  #

cap.release()
out.release()
